
/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

public class CSVHandler2 {

	private String file = "studentName.csv"; // muss sich im ProjektOrdner befinden!
	private String delimiter = ";";
	private String line = "";

	// Constructor 1
	public CSVHandler2() {
	}

	// Constructor 2
	public CSVHandler2(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	// Begin Methods

	public List<Schueler> getAll() {
		Schueler s = null;
		List<Schueler> students = new ArrayList<Schueler>();

		// Add your code here
		try {
			FileReader fr = new FileReader("studentName.csv");
			BufferedReader reader = new BufferedReader(fr);
			String[] wordlist;
			String zeile = reader.readLine(); // liest 1. Zeile

			if (zeile != null) {
				wordlist = zeile.split(";");
				System.out.printf("%-20s %-10s %-10s %-10s %n", "Name", "Joker", "Blamieren", "Frage");
				zeile = reader.readLine();
			}

			while (zeile != null) {

				wordlist = zeile.split(";");
				System.out.printf("%-10s%-10s %-5s %-5s %-5s %n", wordlist[1], wordlist[0], wordlist[2],
						wordlist[3], wordlist[4]);
				zeile = reader.readLine(); // liest neue Zeile ein

			}

		} catch (Exception ex) {
			System.out.println("Fehlermeldung....");
		}

		return students;
	}

	public void printAll(List<Schueler> students) {
		for (Schueler s : students) {
			System.out.println(s.getName());
		}
	}
}