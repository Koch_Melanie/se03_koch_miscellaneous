package Auftrag5;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/* Arbeitsauftrag:  Lesen Sie nur die Autoren des Buches "XQuery Kick Start" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  
 *                   Ausgabe soll wie folgt aussehen:
 *                     Buchtitel:  XQuery Kick Start
 *                     Autoren: 
 *                     	    1. autor: James McGovern
 *                          2. autor: Per Bothner
 *                          3. autor: Kurt Cagle
 *                          4. autor: James Linn
 *                          5. autor: Vaidyanathan Nagarajan
 *                          
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */



public class ReadBookstoreData5 {

	public static void main(String[] args) {

		
			// Name der Datei: "src/Auftrag5/buchhandlung.xml"
			// Add your code here
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse("buchhandlung3_4.xml");
		
		NodeList titelNodes = doc.getElementsByTagName("titel");
		
		for (int i = 0; titelNodes.item(i) != null; i++) {
			
			Node titelNode = titelNodes.item(i);
			if (titelNodes.item(i) == "XQuery Kick Start") {
				System.out.println("Buchtitel: XQuery Kick Start");
			}
			
		}
		
		
				
		


	}

}
