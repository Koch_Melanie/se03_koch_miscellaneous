package Auftrag3;

/* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel" 
*					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
*
*                   Ausgabe soll wie folgt aussehen:
*                    titel:  Java ist auch eine Insel   
*					  vorname:  Christian 
*                    nachname:  Ullenboom 
*/

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("buchhandlung3.xml");

			NodeList titelNodes = doc.getElementsByTagName("titel");
			Node titelNode = titelNodes.item(0);

			NodeList vornameNodes = doc.getElementsByTagName("vorname");
			Node vornameNode = vornameNodes.item(0);
			NodeList nachnameNodes = doc.getElementsByTagName("nachname");
			Node nachnameNode = nachnameNodes.item(0);

			System.out.println(titelNode.getNodeName() + ":" + titelNode.getTextContent());

			System.out.println(vornameNode.getNodeName() + ":" + vornameNode.getTextContent());
			System.out.println(nachnameNode.getNodeName() + ":" + nachnameNode.getTextContent());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
