package Auftrag1;

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData1 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag1/buchhandlung.xml"
			// Add your code here

			// -------------------------------------------------------------------------------------------------------------------------
			// Standardcode zum Einlesen von XML Dateien

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); // 1. Klasse: DocumentBuilderFactory
			DocumentBuilder builder = factory.newDocumentBuilder(); // 2. Klasse: DocumentBuilder
			Document doc = builder.parse("buchhandlung1.xml"); // 3. Klasse: Document

			NodeList titelNodes = doc.getElementsByTagName("titel"); // Es wird eine Liste erstellt, Tagname ist in
																		// diesem Beispiel "Titel" //
			Node titelNode = titelNodes.item(0); // die eingelesen Informationen sollen gespeichert werden.

			System.out.println(titelNode.getTextContent()); // ich m�chte die Informationen, die in der Liste
															// gespeichert sind, ausgeben

			// -------------------------------------------------------------------------------------------------------------------------

			// Erweiterung:

			System.out.println(titelNode.getNodeName() + ":" + titelNode.getTextContent()); // titelNode = <titel>

		} catch (Exception e) {
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

	}

}
