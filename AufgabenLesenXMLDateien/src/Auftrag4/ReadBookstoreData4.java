package Auftrag4;

import javax.xml.parsers.*;
import org.w3c.dom.*;

/* Arbeitsauftrag:  Lesen Sie nur die Buchtitel 
 *					aus der Datei "buchhandlung.xml" und geben Sie sie 
 * 					auf dem Bildschirm aus.
 * 
 *                  Ausgabe soll wie folgt formatiert werden:
 *                     1. titel: Everyday Italian
 *                     2. titel: Harry Potter
 *                     3. titel: XQuery Kick Start
 *                     4. titel: Learning XML
 *                     
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */

public class ReadBookstoreData4 {

	public static void main(String[] args) {

		// Name der Datei: "buchhandlung4_5.xml"
		// Add your code here

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("buchhandlung4_5.xml");

			NodeList titelNodes = doc.getElementsByTagName("titel"); // = NodeListObjekt, liest ALLE Titel aus

			int zaehler = 1; // f�r die Auflistung in der Ausgabe
			
			for (int i = 0; titelNodes.item(i) != null; i++) {

				Node titelNode = titelNodes.item(i);
				System.out.println(zaehler + ". " + titelNode.getNodeName() + ": " + titelNode.getTextContent());
				zaehler++;
			}

		} catch (Exception e) {

			System.out.println("...Fehlermeldung...");
		}

	}

}
