package Auftrag2;

import javax.xml.parsers.*;
import org.w3c.dom.*;

/* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                        titel:  Java ist auch eine Insel 
 *                        autor:  Max Mustermann 
 */

public class ReadBookstoreData2 {

	public static void main(String[] args) {

		try {

			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("buchhandlung2.xml");

			NodeList titelNodes = doc.getElementsByTagName("titel");
			Node titelNode = titelNodes.item(0);

			NodeList autorNodes = doc.getElementsByTagName("autor");
			Node autorNode = autorNodes.item(0);

			System.out.println(titelNode.getNodeName() + ":" + titelNode.getTextContent());
			System.out.println(autorNode.getNodeName() + ":" + autorNode.getTextContent());

		} catch (Exception e) {
		}

	}

}
