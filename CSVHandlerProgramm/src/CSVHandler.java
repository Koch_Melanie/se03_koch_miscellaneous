
/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

public class CSVHandler {

	private String file = "studentName.csv"; // muss sich im ProjektOrdner befinden!
	private String delimiter = ";";
	private String line = "";

	// Constructor 1
	public CSVHandler() {
	}

	// Constructor 2
	public CSVHandler(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	// Begin Methods

	public List<Schueler> getAll() {
		Schueler s = null;
		List<Schueler> students = new ArrayList<Schueler>();

			
		// Add your code here
		try {
		FileReader fr = new FileReader ("studentName.csv");
		
		BufferedReader reader = new BufferedReader(fr);
		
		String zeile = reader.readLine(); // liest 1. Zeile
		
		while (zeile!=null) {
			
			System.out.println(zeile); // gibt Zeile aus
			zeile = reader.readLine(); // liest neue Zeile ein
		}
		
		}
		catch (Exception ex) {
			System.out.println("Fehlermeldung....");
		}

	
		
		
		return students;
	}

	public void printAll(List<Schueler> students) {
		for (Schueler s : students) {
			System.out.println(s.getName());
		}
	}
}