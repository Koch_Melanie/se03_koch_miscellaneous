
/*
 * {
 * 	"titel" : "XQuery kick start",
 *  "verlag" : {
 *               "name" : "Sams",
 *               "ort" : "Indianapolis"
 *              
 *             },
 *   "autoren" : ["autor1", "autor2]
 *  
 * }
 * 
 */

import java.io.*;
import javax.json.*;
import javax.json.spi.*;

public class WriteBookdataJson {

	public static void main(String[] args) {

       	
        // Titel
		JsonObjectBuilder bookBuilder = Json.createObjectBuilder(); // Ich erstelle ein Objekt der Klasse Json
		bookBuilder.add("titel", "Xquery kick start"); // Ich m�chte ein Json-Objekt haben mit folgenden Werten

	
        // Verlag
		JsonObjectBuilder verlagBuilder = Json.createObjectBuilder();
		verlagBuilder.add("name", "Sams");
		verlagBuilder.add("ort", "Indianapolis");
		bookBuilder.add("verlag", verlagBuilder);
		
		//Autoren
		JsonArrayBuilder autorenBuilder = Json.createArrayBuilder ();
		autorenBuilder.add("autor1");
		autorenBuilder.add("autor2");
		bookBuilder.add("autoren", autorenBuilder);
	
		
		JsonObject jo = bookBuilder.build(); // Aus allem was ich dir gegeben habe, mach bitte ein Json-Objekt (jo)

		try {

			FileWriter fw = new FileWriter("book.json"); //
			JsonWriter jw = Json.createWriter(fw); //
			jw.write(jo); // Ich m�chte das Json-Objekt in eine Datei speichern

			fw.close();
			jw.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
