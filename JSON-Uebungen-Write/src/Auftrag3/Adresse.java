package Auftrag3;



public class Adresse {
	
	
	// Attribute
	
	private String strasse;
	private String hausnummer;
	private int plz;
	private String ort;
	
	// Konstruktur
	
	public Adresse (String strasse, String hausnummer, int plz, String ort) {
		
		this.strasse = strasse;
		this.hausnummer = hausnummer;
		this.plz = plz;
		this.ort = ort;
	}
	
	// Getter & Setter
	
	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getHausnummer() {
		return hausnummer;
	}

	public void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public int getPlz() {
		return plz;
	}

	public void setPlz(int plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	
				
		
		
		
		
	}
	
	

