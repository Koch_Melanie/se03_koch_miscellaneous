package Auftrag3;

import javax.json.Json;

/* Auftrag 3) Erstellen Sie notwendige Klassen und erstellen Sie folgende buchhandlung3.json:
{
	"name": "OSZIMT Buchhandlung",
	"tel": "030-225027-800",
	"fax": "030-225027-809",
	"adresse": {
		"strasse": "Haarlemer Strasse",
		"hausnummer": "23-27",
		"plz": "12359",
		"ort": "Berlin"
	}
}

*/

import java.io.*;
import javax.json.*;
import javax.json.spi.*;



public class WriteBookstoreDataJason {

	public static void main(String[] args) {
		
		Adresse a1 = new Adresse ("Haarlemer Strasse", "23-27", 12359, "Berlin");

		JsonObjectBuilder buchhandlungBuilder = Json.createObjectBuilder();
		buchhandlungBuilder.add("name", "OSZIMT Buchhandlung");
		buchhandlungBuilder.add("tel", "030-225027-800");
		buchhandlungBuilder.add ("fax", "030-225027-809");
		
		JsonObjectBuilder adresseBuilder = Json.createObjectBuilder();
		adresseBuilder.add("strasse", a1.getStrasse());
		adresseBuilder.add("hausnummer", a1.getHausnummer());
		adresseBuilder.add("plz", a1.getPlz());
		adresseBuilder.add("ort", a1.getOrt());
		
		
		buchhandlungBuilder.add ("adresse", adresseBuilder);
		
		
		JsonObject JsonObject = buchhandlungBuilder.build();
		
		
		
		try {
			
			FileWriter fw = new FileWriter ("buchhandlung3.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(JsonObject);
			
			fw.close();
			jw.close();
			
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
