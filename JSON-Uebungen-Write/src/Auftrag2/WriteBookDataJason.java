package Auftrag2;

/* Auftrag 2) Erstellen Sie folgende book2.json:
{
	"titel": "Java ist auch eine Insel",
	"jahr": 1998,
	"preis": 29.95,
	"autor": "Christian Ullenboom"
}
*/

import java.io.*;
import javax.json.*;
import javax.json.spi.*;

public class WriteBookDataJason {

	public static void main(String[] args) {

		Buch b1 = new Buch("Java ist auch eine Insel", 1998, 29.95, "Christian Ullenboom");
		
		
		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("titel", "Java ist auch eine Insel");
		bookBuilder.add("jahr", 1998);
		bookBuilder.add("preis", 29.95);
		bookBuilder.add("autor", "Christian Ullenboom");
		
		JsonObject JsonObject = bookBuilder.build();
		
		try {
			
			FileWriter fw = new FileWriter ("book2.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(JsonObject);
			
			fw.close();
			jw.close();
			
		}
		catch (Exception e) {
			
			e.printStackTrace ();
		}

		

	}

}
