import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* Arbeitsauftrag:  Lesen Sie den Titel des Buches "Java ist auch eine Insel" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  Ausgabe soll wie folgt aussehen:
 *                     titel:  Java ist auch eine Insel                  
 */




public class ReadBookstoreData1 {

	public static void main(String[] args) {

		try {
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = factory.newDocumentBuilder();
			Document doc = docBuilder.parse("buchhandlung.xml");
			
			
//			Node titel = doc.getElementsByTagName("titel"); = Auslesen aller Titel
			
			Node titel = doc.getElementsByTagName("titel").item(0); // Auslesen des 1. Titel
			Element titelElem = (Element) titel; // umcasten 
			
			Node autor = doc.getElementsByTagName("autor").item(0);
			Element autorElem = (Element) autor;
			
			
			System.out.println("titel: " + titelElem.getTextContent()); // Ausgabe des Titels, der in der Datei drinsteht
			System.out.println("autor: " + autorElem.getTextContent());
			
			
			
			
		}
		catch (Exception ex) {
			System.out.println("Fehlermeldung");
		}

	}

}
