//---------------------------------------------------
// IMMER importieren, wenn wir mit XML-Dateien arbeiten:

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.xml.transform.*;
import org.w3c.dom.*;

//---------------------------------------------------

public class WriteBookstoreData1 {

	public static void main(String[] args) {
	
		try {
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();
			
			Element buecher = doc.createElement("buecher"); // das Element "B�cher" wird erstellt, die Verbindung zum Document "doc" besteht noch nicht!
			doc.appendChild(buecher); // das Document "B�cher" h�ngt nun unter dem Document "doc"
			
			Element buch = doc.createElement("buch");
			buecher.appendChild(buch);
			
			Element titel = doc.createElement("titel");
			buch.appendChild(titel);
			
			titel.appendChild(doc.createTextNode("Java ist auch eine Insel")); // das ist nun ein neues Text-Element
			
			// der "Baum" ist jetzt fertig
			
			
			// ..............................................
			// Vorbereiten f�r Schreiben in DAtei
			
			TransformerFactory tfactory = TransformerFactory.newInstance();
			Transformer transformer = tfactory.newTransformer();
			
			DOMSource source = new DOMSource(doc); // das Dokument was ich erstellt habe (doc) �bergebe ich einem Objekt der Klasse "DOMSoure"
			StreamResult result = new StreamResult("buchhandlung1.xml"); // Die Daten sollen in einer Datei gespeichert werden, die buchhandlung1.xml hei�en soll
			
			transformer.transform(source, result);
				
			
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
