/* Arbeitsauftrag:  Speichern Sie die Liste der B�cher 
 * 					(buchliste) in die Datei "buchhandlung.xml".
 * 					Dabei gehen Sie wie folgt vor:
 *                  - Erstellen Sie ein DOM-Dokument  
 * 					- sichern Sie es als XML in die Datei "buchhandlung.xml".
 * 
 * 	Hinweis: Die Struktur der Ergebnisdatei soll der Datei 
 *           "Vorgabe_f�r_Ausgabedatei.xml" entsprechen. 			
 *               
 */
package Auftrag3;


import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData3 {

	public static void main(String[] args) {

		List<Buch> buchliste = new ArrayList<>();
		buchliste.add(new Buch("Everyday Italian", "Giada De Laurentiis", 30.0));
		buchliste.add(new Buch("Harry Potter", "J K. Rowling", 29.99));
		buchliste.add(new Buch("XQuery Kick Start", "James McGovern", 49.99));
		buchliste.add(new Buch("Learning XML", "Erik T. Ray", 39.95));

		// Add your code here

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();
			
			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung);
			
			Element buch = doc.createElement("buch");
			buchhandlung.appendChild(buch);
			
			Element titel = doc.createElement("titel");
			buch.appendChild(titel);
			
			Element autor = doc.createElement("autor");
			buch.appendChild(autor);
			
			Element preis = doc.createElement("preis");
			buch.appendChild(preis);
			
			for (int i = 0; i < buchliste.size(); i++) {
				
				titel.appendChild(doc.createTextNode(buchliste.get(i).getTitel()));
				buch.appendChild(titel);
				autor.appendChild(doc.createTextNode(buchliste.get(i).getAutor()));
				preis.appendChild(doc.createTextNode("" + buchliste.get(i).getPreis()));

			}
			

			
//			
			
			
			TransformerFactory tfactory = TransformerFactory.newInstance();
			Transformer transformer = tfactory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult("buchhandlung.xml");
			transformer.transform(source, result);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
